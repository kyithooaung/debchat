# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :debchat,
  ecto_repos: [Debchat.Repo]

# Configures the endpoint
config :debchat, DebchatWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "1T1IxkO2r60zYVLNrn1R380o4lp4+V4MtQ5taqnf4V+Z0Ix/zk5Y4IzKq1m5rMR2",
  render_errors: [view: DebchatWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Debchat.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
